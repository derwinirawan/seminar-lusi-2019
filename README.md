# Seminar LUSI (Lumpur Sidoarjo) 19 September 2019

[**Versi PDF**](https://mfr.au-1.osf.io/render?url=https://osf.io/bhxqw/?action=download%26mode=render)

[**Materi tayangan**](https://bit.ly/2mjnJ2V)

# Pendahuluan

Beberapa catatan dari acara Seminar LUSI 2019 di Ibis Trans Studio Mal Bandung 19 September 2019.
Tujuan: untuk mempublikasikan perkembangan LUSI.
Narasumber:
1. Prof.Dr.Ir. Hardi Prasetyo: Sejarah penanganan LUSI
2. Handoko Teguh Wibowo, ST, MSi: Dinamika geologi LUSI
3. Dr.Ir. Ria Asih Aryani Soemitro, M.Eng: Geoteknik dan infrastruktur sekitar LUSI
4. Prof. Iwan Kridasantausa: Pengaruh LUSI terhadap sungai dan muara
5. Dr.Ir. Heryadi Rachmat: Pengembangan geowisata LUSI
6. Lasino, ST.APU: Prospek pemanfaatan LUSI

# Beberapa catatan

## Prof. Hardi Prasetyo

Membahas secara umum lini masa LUSI sejak awal, sudah ditayangkan di [blog pribadinya](https://hardiprasetyolusi.wordpress.com/), dengan penekanan:

- informasi geologi penting untuk pengambilan keputusan, karena saat ini LUSI akan sangat berkaitan dengan: penataan wilayah masa depan, LUSI sebagai *geoheritage*, LUSI sebagai material yang memiliki manfaat.
- informasi perlu data. Data perlu diinventarisasi.
 
Berikut catatan visualnya.

![catatan visual hardi prasetyo](https://lh3.googleusercontent.com/COedO3G0g9WoS0pH4x1n6BNZxuB9iDu8GvmFBn4Xw3oKU1ECgbZJ6SccaSmebTjVHb9u-j935ir14ivf5cuolRp1CDW45MRyyMMRtgqiHudsjjiO9t6DAS1aAx1ibQBGEXyqvkjazjfeAwSnbEvUr-YFsweU4vNiCapg9E7PyUV3dBhKqG0U5XKrCx8_2xSJ6SnmevTBUg1LWN3clxGrIHIGGPzt6TyLQaRIe5wUQx3c1YNC-onWningASL2bDDsCJ4FWSe9n5p3u64j1V-FPFwAr7swnObKFXVuejXmzfS53-AlsAn3cccWDmpgJcdV-jaLWkxRvvtY9IBNHS-BuTqSwzWXUDpafqZn-LYD_SOzl3LNPMdnmJiAlh7gjfSABBnX8tWyn3YZ7H4KgtPES9qcpw6-METlLWWoyzpc9usAKs5f78kEw1Nvf-dP4FC2CxUhcsBazcKBuFjexRUdyuSPCtTQW6UEoqwbJmQaspkyBR2MAvDk9zuBt25mgAa16gdemJhm7qGydp0NDO9i4nyAannIQ0qDWzJMEivRebIOimHqtlj9ME2KKz5ytuCKvZx0IWCZCWVAecD7D12DzkCC3C2AHrgtKk-hUMpSO2AHS6PhiEXckXzn1snWWiatEsqPMJCOYEaNm9AflVW0uO68lskJzNqFs6Xm-zeZutzZrsGYmNmehg=w2016-h1364-no)
[Tautan](https://photos.app.goo.gl/CkQhWUrkC7e5nQJS7) **Lisensi CC-BY (bebas unduh-bebas pakai)**

Catatan tambahan:
Bapak Hardi pernah melakukan peninjauan terbuka untuk makalah yang membahas LUSI. Hasil peninjauan [ditayangkan di sini](https://hardiprasetyolusi.wordpress.com/2019/04/30/agu2019-evaluasi-isi-sistem-hidrotermal-lusi/).

## Handoko Teguh Wibowo, ST, MSi

Menjelaskan ringkasan (overview):

- kilas balik LUSI: proses kejadian, fenomena-fenomena yang terjadi, runtuhnya tanggul cincin, dst sampai sekarang.
- kondisi geologi regional: sistem batuan sedimen, batuan gunungapi, dan sesar-sesar yang ada di sekitarnya.
- pengaruhnya kepada lingkungan purba, yaitu perkembangan kerajaan yang ada di sekitarnya.

Berikut catatan visualnya.

![catatan visual handoko teguh](https://lh3.googleusercontent.com/dr3zfMQt1-wB3n4bIsviBpMdBUTdMU-_FupCR6H4RsUihMsFdaHYVzGMTyPW6Aq41T0bYIeHn149CIgfzUeQHw0q9MQH1RZqC7A0o61N80z65pAJKmthi5KB-_SBYcKanGR6_aQGXp53PQ65ECxCLhYHyD6vDQIlp71HMXFYwx3iGpN7J1D7bf1JhrngxxCw89UzQTKcWzDCLGh_64R3ijP0Os6BWu5Z6z45trC5yCukzHi7Wumq3iOxr-TSQCrKGNensvKzt7DoFtlBNVk4ikydido1I-wXsu4gFTeTQZBmI76FKzaWDAXT1EdpAiE1ctyc7y72RiLzUIrdEIuougYSRETpv_Eg1yJ82vtVx483sDrVOMnSU6PlHqYhT4IfYaexySZlEEMRpCgVPJQJPusKB8kQMXBneAjjqHHYUfYDVm9awx6FZ5WKgEyhMuvYZzCxE4LmwhyE2FrXx2_CwuzBga0HBVF6oukdqYpOxGqvnxiM2XdkgemwktfzoOpXYNTE45l-aQzdWqloseuLaYV4rHeu7Rbp9aVAXYOOoQYBUGH59N_m-89G8PqsH9kmhSoM3_S4DXH1_lABugdz9LylsbGRF7RK9pItzgiEdT8nvPod8f9ZrWy45rQKotziknqeSt-8RY-zGfQxDxcvQ_OLQVrRCAeTpK5g7u_NLWGP_oLHdcqN0w=w2060-h1364-no)[Tautan](https://photos.app.goo.gl/MKrVAxAXUDD2oXtW7) **Lisensi CC-BY (bebas unduh-bebas pakai)**

## Dr. Ria Asih Aryani Soemitro

Membahas ketahanan tanggul dari sudut pandang teknik sipil:

- bahwa tanah di lokasi sampai kedalaman 20 an meter adalah tanah lunak dengan nilai SPT 1-3. Ini mengindikasikan bahwa tanah tidak memiliki kekuatan, apalagi untuk pondasi tanggul.
- bahwa stabilitas tanggul perlu juga memperhatikan *puncture stability*.
- bahwa kondisi bantaran sungai di sekitar area juga memiliki pengaruh ke lokasi.

Berikut catatan visualnya.

![catatan visual ria asih](https://lh3.googleusercontent.com/8NEq6Iz4elORtJJXCeD8fyXvkGPlGOT2zo3B9X1IlGFI7A6hBRdD5Ig0RxcpaOWddyO6rNz3xc_gYyP9r_WPr8NJgpmGpizNcOc13WuUgMjbKRXIW85E2_f2pbtZYV8IK_9RQZTFFg0YWn_QnwW0JiqUtBrA8TCyh897ui1q2qSu0E27jiHaIwks9buuamrrFKfHAbdTyES33xunjuYl5xmgHaPa9fePSJpDzxyoYtmtG3IjnaKXGsF0OB0BLVQyQpObB2wjdNdgyrfyHJ8c2ufWVWFJ1xkwlN9E9qZvPUxQbU1hcfkSyoHpaV5x-IeV_xXbcv8yX3tVdn5Arg7S8IMHzMJlpNOUYQxWPrWRB1zCX4uaFsyy0UI1WIihM0MRBbmFF92EZWtzy_mAE8FESQHQWOMZb1L_nbEgKDOTDzrY1jEsGHGF4IrpRKAO3NR4S3eBbmXT8bNMbwgFKl9COoSInSotHWeq3joeVrNA4wao093HRFWhH1Za_kXD4KkF65jAuctRC3hkQnwuAqg6UzA8inUJh7PjYmACsBfQ4ChtRp0KnDGsv7yeh4CEP97wJYnCKPGIo6BYfCBQow0IVM_S_LF6eRAmmLbJ85wOXY02CUbpaEQKuCJQ9OJXjdEJTmmKmIDPIeNuocuJke2HZ6eHMJtd1e8-JFZ-rj4uPidkPjaOxLEilQ=w2172-h1364-no)

[Tautan](https://photos.app.goo.gl/Vi51zQdmkAhzvubj6) **Lisensi CC-BY (bebas unduh-bebas pakai)**  

# Sekilas bibliometrik karya ilmiah tentang LUSI

## Kata kunci: "Lumpur Sidoarjo"

Basis data yang digunakan: [Dimensions](https://app.dimensions.ai/discover/publication)

Kata kunci: `"Lumpur Sidoarjo"`, 

Modus: `full data search`

Tautan pencarian: https://app.dimensions.ai/discover/publication?search_text=%22Lumpur%20sidoarjo%22&search_type=kws&search_field=full_search

Hasil: 192 dokumen

Berkas: seluruh berkas pendukung dapat diunduh [di sini](https://osf.io/kyxpj/).

Bagaimana fluktuasi makalah berdasarkan tahun?

![makalah berdasarkan tahun](https://mfr.au-1.osf.io/export?url=https://osf.io/3f2cg/?action=download%26mode=render%26direct%26public_file=True&initialWidth=851&childId=mfrIframe&parentTitle=OSF+%7C+lumpur-chart-by-year.png&parentUrl=https://osf.io/3f2cg/&format=2400x2400.jpeg)
Bagaimana makalah berdasarkan jumlah sitasi?

![makalah berdasarkan jumlah sitasi](https://mfr.au-1.osf.io/export?url=https://osf.io/wq2b5/?action=download%26mode=render%26direct%26public_file=True&initialWidth=851&childId=mfrIframe&parentTitle=OSF+%7C+lumpur-chart-by-citations.png&parentUrl=https://osf.io/wq2b5/&format=2400x2400.jpeg)
Siapa saja yang pernah menulis (10 teratas)?

![makalah menurut penulis](https://mfr.au-1.osf.io/export?url=https://osf.io/m2f3e/?action=download%26mode=render%26direct%26public_file=True&initialWidth=851&childId=mfrIframe&parentTitle=OSF+%7C+lumpur-chart-by-authors.png&parentUrl=https://osf.io/m2f3e/&format=2400x2400.jpeg)
Jurnal apa saja yang memuat makalah tentang LUSI (10 teratas)?

![by journals](https://mfr.au-1.osf.io/export?url=https://osf.io/uj9y5/?action=download%26mode=render%26direct%26public_file=True&initialWidth=851&childId=mfrIframe&parentTitle=OSF+%7C+lumpur-chart-by-journals.png&parentUrl=https://osf.io/uj9y5/&format=2400x2400.jpeg)
Makalah-makalah tersebut termasuk ke dalam lingkup bidang ilmu apa saja?

![by fields](https://mfr.au-1.osf.io/export?url=https://osf.io/vumt2/?action=download%26mode=render%26direct%26public_file=True&initialWidth=851&childId=mfrIframe&parentTitle=OSF+%7C+lumpur-chart-by-field.png&parentUrl=https://osf.io/vumt2/&format=2400x2400.jpeg)

## Kata kunci: "LUSI mud volcano"

Basis data yang digunakan: [Dimensions](https://app.dimensions.ai/discover/publication)

Kata kunci: `"LUSI mud volcano"`, 

Modus: `full data search`

Tautan pencarian: https://app.dimensions.ai/discover/publication?search_text=%22LUSI%20mud%20volcano%22&search_type=kws&search_field=full_search

Hasil: 357 dokumen

Berkas: seluruh berkas pendukung dapat diunduh [di sini](https://osf.io/kyxpj/).

Bagaimana fluktuasi makalah berdasarkan tahun?

![makalah berdasarkan tahun](https://mfr.au-1.osf.io/export?url=https://osf.io/ugav2/?action=download%26mode=render%26direct%26public_file=True&initialWidth=851&childId=mfrIframe&parentTitle=OSF+%7C+LUSI-chart-by-year.png&parentUrl=https://osf.io/ugav2/&format=2400x2400.jpeg)
Bagaimana makalah berdasarkan jumlah sitasi?

![makalah berdasarkan jumlah sitasi](https://mfr.au-1.osf.io/export?url=https://osf.io/a9bsp/?action=download%26mode=render%26direct%26public_file=True&initialWidth=851&childId=mfrIframe&parentTitle=OSF+%7C+LUSI-chart-by-citations.png&parentUrl=https://osf.io/a9bsp/&format=2400x2400.jpeg)
Siapa saja yang pernah menulis (10 teratas)?

![makalah menurut penulis](https://mfr.au-1.osf.io/export?url=https://osf.io/w5qza/?action=download%26mode=render%26direct%26public_file=True&initialWidth=851&childId=mfrIframe&parentTitle=OSF+%7C+LUSI-chart-by-authors.png&parentUrl=https://osf.io/w5qza/&format=2400x2400.jpeg)
Jurnal apa saja yang memuat makalah tentang LUSI (10 teratas)?

![by journals](https://mfr.au-1.osf.io/export?url=https://osf.io/r9e5b/?action=download%26mode=render%26direct%26public_file=True&initialWidth=741&childId=mfrIframe&parentTitle=OSF+%7C+LUSI-chart-by-journals.png&parentUrl=https://osf.io/r9e5b/&format=2400x2400.jpeg)
Makalah-makalah tersebut termasuk ke dalam lingkup bidang ilmu apa saja?

![by fields](https://mfr.au-1.osf.io/export?url=https://osf.io/37y8a/?action=download%26mode=render%26direct%26public_file=True&initialWidth=741&childId=mfrIframe&parentTitle=OSF+%7C+LUSI-chart-by-fields.png&parentUrl=https://osf.io/37y8a/&format=2400x2400.jpeg)


[**Versi PDF**](https://mfr.au-1.osf.io/render?url=https://osf.io/bhxqw/?action=download%26mode=render)

[**Materi tayangan**](https://bit.ly/2mjnJ2V)
